package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.example.model.Todo;
import com.example.repository.ToDoRepository;


//@EntityScan("com.example.model") 
//@EnableMongoRepositories("com.example.repository.mongodb.crud.repository") 

@EnableMongoRepositories ("com.example.repository") // this fix the problem
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.example.*", "com.example.repository.ObjectiveRepository"}) 
@EntityScan(basePackages = "com.example.*")
@EnableJpaRepositories("com.example.repository")
@Configuration
@SpringBootApplication
//@SpringBootApplication
//@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
//@ComponentScan(basePackages = {"es.unileon.inso2"})
//@EntityScan("es.unileon.inso2.model")

public class LaunchPadApplication implements CommandLineRunner {
	
	
	@Autowired
	  private ToDoRepository repo;
	
	public static void main(String[] args) {
		SpringApplication.run(LaunchPadApplication.class, args);
		System.out.println("Running Application Successfully !");
	}
	
	
	// Feed JOB MONGO DB
	
	@Override
	  public void run(String... args) throws Exception {
	    
		// save a couple of customers
	    repo.save(new Todo("sunday", "dnjkdnana","974747584"));
	    repo.save(new Todo("1845", "Smith", "9303232393"));

	    // fetch all customers
	    
	    System.out.println("Customers found with findAll():");
	    System.out.println("-------------------------------");
	    for (Todo customer : repo.findAll()) {
	      System.out.println(customer);
	    }
	    System.out.println();

	    // fetch an individual customer
	    
	    System.out.println("Customer found with findByFirstName('Alice'):");
	    System.out.println("--------------------------------");
	    System.out.println(repo.findById("sunday"));

	  }

	
}
