package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Objectives;

@Repository
public interface ObjectiveRepository extends JpaRepository<Objectives, String>{
  
	void delete(Objectives deleted);
    List<Objectives> findAll();
    Optional<Objectives> findById(String id);
	@SuppressWarnings("unchecked")
	Objectives save(Objectives objectives);
}	
