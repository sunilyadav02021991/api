package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.example.model.Todo;

public interface ToDoRepository extends MongoRepository<Todo, String>{
	
	void delete(Todo deleted);
    List<Todo> findAll();
    Optional<Todo> findById(String id);
	@SuppressWarnings("unchecked")
	Todo save(Todo todo);
	
	// User define queries
	
	@Query(value = "{'name': ?0}")
    Todo findTodoByMobile(String name);
}
