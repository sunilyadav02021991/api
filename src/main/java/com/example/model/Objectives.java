package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity // This tells Hibernate to make a table out of this class
@Table(name = "objectives")
public class Objectives {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  
  private String title;
  
  private String description;

  
  public Objectives() {
	super();
	// TODO Auto-generated constructor stub
  }

  public Objectives(Long id, String title, String description) {
	super();
	this.id = id;
	this.title = title;
	this.description = description;
  }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Objectives [id=" + id + ", title=" + title + ", description=" + description + "]";
	}
}
