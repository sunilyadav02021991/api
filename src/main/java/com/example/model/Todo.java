package com.example.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

//import static com.example.demo.util.PreCondition.notEmpty;
//import static com.example.demo.util.PreCondition.notNull;

@Document("Todo")
public class Todo {
	static final int MAX_LENGTH_DESCRIPTION = 500;
    static final int MAX_LENGTH_TITLE = 100;
 
    @Id
    private String id;
    @Indexed
    private String name;
 
    private String mobile;
    
	public Todo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Todo(String id, String name, String mobile) {
		super();
		this.id = id;
		this.name = name;
		this.mobile = mobile;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public static int getMaxLengthDescription() {
		return MAX_LENGTH_DESCRIPTION;
	}

	public static int getMaxLengthTitle() {
		return MAX_LENGTH_TITLE;
	}

	@Override
	public String toString() {
		return "Todo [id=" + id + ", name=" + name + ", mobile=" + mobile + "]";
	}    
}
