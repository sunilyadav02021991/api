package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.example.model.Todo;
import com.example.repository.ToDoRepository;


@Configuration
@Service
public class ToDoServiceImplementation {
	
	@Autowired
	private ToDoRepository repo;
	 
	public List<Todo> getUserList() {
		return repo.findAll().stream().map(u -> {
			Todo vo = new Todo();
			vo.setId(u.getId());
			vo.setName(u.getName());
			vo.setMobile(u.getMobile());
			return vo;
		}).collect(Collectors.toList());
	}
	
	public Todo getUserById(String id) {
		Todo user = new Todo();
		user = repo.findById(id).map(u -> {
			Todo vo = new Todo();
			vo.setId(u.getId());
			vo.setName(u.getName());
			vo.setMobile(u.getMobile());
			return vo;
		}).orElse(null);
		return (user == null ? new Todo() : user);
	}
	
	public Todo saveUser(Todo vo) {
		Todo user = new Todo();
		user.setName(vo.getName());
		user.setMobile(vo.getMobile());
		return repo.save(user);
	}
	
	public Todo updateUser(Todo vo) {
		Todo user = new Todo();
		user.setId(vo.getId());
		user.setName(vo.getName());
		user.setMobile(vo.getMobile());
		repo.save(user);
		return user;
	}
	
	public Todo deleteUser(String userid) {
		Todo user = new Todo();
		user.setId(userid);
		Optional<Todo> findUser = repo.findById(userid);
		user.setName(findUser.get().getName());
		user.setMobile(findUser.get().getMobile());
		System.out.println("delete side" + user);
		repo.delete(user);
		return user;
	}
	
	public Todo findTodoByMobile(String mobileNo) {
		Todo user = new Todo();
		user = repo.findTodoByMobile(mobileNo);
		System.out.println("user by mobile" + user);
		return user;
	}
	
}	
