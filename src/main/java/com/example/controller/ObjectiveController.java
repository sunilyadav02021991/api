package com.example.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Objectives;
import com.example.repository.ObjectiveRepository;

@RestController
@RequestMapping(path="/demo") // This means URL's start with /demo (after Application path)
public class ObjectiveController {
	
	  @Autowired // This means to get the bean called userRepository
	  private ObjectiveRepository objectiveRepo;

	  @PostMapping(path="/add") // Map ONLY POST Requests
	  public @ResponseBody String addNewUser (@RequestBody Objectives objectives) {
	    // @ResponseBody means the returned String is the response, not a view name
	    // @RequestParam means it is a parameter from the GET or POST request
		 Objectives obj  = new Objectives();
		 obj.setId(objectives.getId());
		 obj.setTitle(objectives.getTitle());
		 obj.setDescription(objectives.getDescription());
	    objectiveRepo.save(obj);
	    return "Saved Successfully";
	  }
	  
	  @GetMapping(path="/all")
	  public @ResponseBody Iterable<Objectives> getAllUsers() {
		// This returns a JSON or XML with the users
	    return objectiveRepo.findAll();
	  }
	  
	  @GetMapping(path="/user/{userid}")
	  public @ResponseBody Optional<Objectives> getUser(@PathVariable String userid) {
	    // This returns a JSON or XML with the users
	    return objectiveRepo.findById(userid);
	  }
	  
}
