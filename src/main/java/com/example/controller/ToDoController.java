package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Todo;
import com.example.service.ToDoServiceImplementation;

import ch.qos.logback.core.status.Status;

@RestController
public class ToDoController {

	
	@Autowired
	private ToDoServiceImplementation todoservice;
	
	@GetMapping("/checkmessage") 
	public String check() {
		return "Hello World";
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<Todo> getUser(@PathVariable String id) {
		Todo user = new Todo();
		user = todoservice.getUserById(id);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	@GetMapping("/user")
	public ResponseEntity<List<Todo>> getUserList() {
		return new ResponseEntity<>(todoservice.getUserList(), HttpStatus.OK);
	}
	@PostMapping("/user")
	public ResponseEntity<Todo> saveUser(@RequestBody Todo UserVo) {
		System.out.println(UserVo);
		// Todo createdObj = todoservice.saveUser(UserVo);
		return new ResponseEntity<>(todoservice.saveUser(UserVo), HttpStatus.OK);
	}
	
	@PutMapping("/user/{userid}")
	public ResponseEntity<Todo> updateUser(@RequestBody Todo UserVo, @PathVariable String userid) {
		UserVo.setId(userid);
		
		return new ResponseEntity<Todo>(todoservice.updateUser(UserVo), HttpStatus.OK);
	}
	@DeleteMapping("/user/{userid}") 
	public ResponseEntity<Todo> deleteUser(@PathVariable String userid) {
		System.out.println(userid);
		return new ResponseEntity<Todo>(todoservice.deleteUser(userid), HttpStatus.OK);
	}
	
	// User define query firing 
	
	@GetMapping("/user/mobile/{mobileNo}")
	public ResponseEntity<Todo> getUserByMobile(@PathVariable String mobileNo){
		return new ResponseEntity<Todo>(todoservice.findTodoByMobile(mobileNo) == null ? new Todo() : todoservice.findTodoByMobile(mobileNo), HttpStatus.OK);
	}
	
	
	
	
}	
